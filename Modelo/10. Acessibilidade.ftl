﻿<#if ac_contraste_nome.ac_contraste.getData() == "true" || ac_link_titulo_1.getData()?has_content || ac_link_titulo_2.getData()?has_content>
    <h3 class="hide">${.vars['reserved-article-title'].data}</h3>
   <ul>
      <#if ac_link_titulo_1.getData()?has_content>
         <li>
            <a href="${ac_link_titulo_1.ac_link_url_1.getData()}" title="${ac_link_titulo_1.getData()}" target="${ac_link_titulo_1.ac_link_target_01.getData()}" accesskey="5">${ac_link_titulo_1.getData()}</a>
         </li>
      </#if>
      <#if ac_contraste_nome.ac_contraste.getData() == "true">
         <li>
            <a href="#" id="alto_contraste_id" class="alto_contraste contraste_off" title="${ac_contraste_nome.getData()}" accesskey="6">${ac_contraste_nome.getData()}</a>
         </li>
      </#if> 
      <#if ac_link_titulo_2.getData()?has_content>
         <li>
            <a href="${ac_link_titulo_2.ac_link_url_2.getData()}" title="${ac_link_titulo_2.getData()}" target="${ac_link_titulo_2.ac_link_target_02.getData()}" accesskey="7">${ac_link_titulo_2.getData()}</a>
         </li>
      </#if> 
   </ul>

</#if>