﻿<#if linkTitulo.getSiblings()?has_content>
    <h3 class="hide">Serviços</h3>
	<ul class="listagem-links">
		<#list linkTitulo.getSiblings() as cur_linkTitulo>
			<li class="link-item">
				<a href="${cur_linkTitulo.linkUrl.getData()}" title="${cur_linkTitulo.getData()}" target="${cur_linkTitulo.linkTarget.getData()}">${cur_linkTitulo.getData()}</a>
			</li> 
		</#list>
	</ul>
</#if>