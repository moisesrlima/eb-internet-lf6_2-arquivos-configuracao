﻿<div class="botao-imagem-box row-fluid">
<#if titulo_do_link.getSiblings()?has_content>
	<#list titulo_do_link.getSiblings() as cur_titulo_do_link>
	    <a class="botao-imagem span4" href="${cur_titulo_do_link.endereço_da_pagina.getData()}" style="background-image: url(${cur_titulo_do_link.imagem_de_fundo.getData()})">
	        <div>
	            <span>${cur_titulo_do_link.getData()}</span>
	       </div>
		</a>
	</#list>
</#if>
</div>